# Developer Applicant Interview Test

Este proyecto contiene el codigo solicito en la prueba tecnica enviada:

## Iniciemos 🚀

Mira **Despliegue** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Node / Npm_

### Instalación 🔧

_Instalar dependencias del back desde el root ejecutar_

```
npm install
```

_Instalar dependencias del front desde la carpeta client ejecutar_

```
npm install
```

## Ejecutando las pruebas ⚙️

_Las pruebas fueron realizadas con mocha / chai / supertest para el back, desde el root_

```
npm run test
```

## Despliegue 📦

_Despliegue front, desde la carpeta client, ejecutar:_

```
npm run build
```

_Despliegue back, desde la carpeta root, ejecutar:_

```
npm run build
```

## Autor ✒️

* **Roberto Gonzalez** - *Developer* - [rogonzalez](https://github.com/rogonzalez88)