'use strict';

const Service = require('../services/transaction.service').instance;
const ResponseError = require('../helpers/ResponseError');

let Controller = {
  getById: async (req, res, next) => {
    try {
      const transaction = await Service.getTransactionById(req.params.id);
      if (transaction) {
        return res.status(200).send(transaction);
      }
      next(new ResponseError(response.statusMessage, response.statusCode));
    } catch (e) {
      next(e);
    }
  },
  create: async (req, res, next) => {
    try {
      const transaction = await Service.commitTransaction(req.body);
  
      res.status(201).send(transaction);
    } catch (e) {
      next(e);
    }
  },
  getHistory: async (req, res, next) => {
    try {
      const history = await Service.getTransactionHistory();
  
      res.status(200).send(history);
    } catch (e) {
      next(e);  
    }
  },
  getBalance: async (req, res, next) => {
    try {
      const amount = await Service.getCurrentAmount();
      res.status(200).send({ amount });
    } catch (e) {
      next(e);
    }
  }
};

module.exports = Controller;