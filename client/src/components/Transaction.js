import React from 'react';
import moment from 'moment'

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import './Transaction.css';

function Transaction({ data }) {
  console.log(data);
  return (
    <ExpansionPanel className={`panel-${data.type}`}>
      <ExpansionPanelSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography>Type: {data.type} - Amount: {data.amount}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <div>
          <div>
            Type: {data.type}
          </div>
          <div>
            Amount: {data.amount}
          </div>
          <div>
            Date: {moment(data.effectiveDate).format('DD-MM-YYYY')}
          </div>
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default Transaction;