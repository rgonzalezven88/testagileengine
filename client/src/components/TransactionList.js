import React, { Component } from 'react';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';

import Transaction from './Transaction'
import './TransactionList.css';

class TransactionList extends Component {

  constructor() {
    super();
    this.state = {
      transactions: []
    }
  }

  componentDidMount() {
    axios.get(`http://localhost:8080/api/transactions`)
      .then(res => {
        const transactions = res.data;
        this.setState({ transactions });
      })
  }

  render() {
    if (!this.state.transactions.length > 0) {
      return <CircularProgress/>
    } else {
      return (
        <div className="transaction-list">
          {this.state.transactions.map((item) => (
            <Transaction data={item} key={item.id} />
          ))}
        </div>
      );
    }
  }
}

export default TransactionList;
