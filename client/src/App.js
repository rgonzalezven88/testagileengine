import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Container from '@material-ui/core/Container';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import TransactionList from './components/TransactionList';

import './App.css';

class App extends Component {

  render() {
    return [
      <AppBar position="static" key="header">
        <Toolbar variant="dense">
          <Typography variant="h6" color="inherit">
            Agile Engine
          </Typography>
        </Toolbar>
      </AppBar>,
      <Container className="full-container" key="container">
        <TransactionList></TransactionList>
      </Container>
    ];
  }
}

export default App;
