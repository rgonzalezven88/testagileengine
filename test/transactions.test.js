'use strict';

const app = require('../app'),
  chai = require('chai'),
  request = require('supertest');
const expect = chai.expect;

let transactionId;

describe('Transaction Api Tests', function () {
  it('should get current balance', function (done) {
    request(app)
      .get('/api/')
      .end(function (err, res) {
        expect(res.statusCode).to.equal(200);
        expect(res.body.amount).to.equal(0);
        done();
      });
  });
  it('should cant create with invalid type', function (done) {
    request(app)
      .post('/api/transactions')
      .send({
        type: 'test',
        amount: 400
      })
      .end(function (err, res) {
        expect(res.statusCode).to.equal(400);
        done();
      });
  });
  it('should create debit transaction', function (done) {
    request(app)
      .post('/api/transactions')
      .send({
        type: 'debit',
        amount: 400
      })
      .end(function (err, res) {
        expect(res.statusCode).to.equal(201);
        transactionId = res.body.id;
        done();
      });
  });
  it('should create credit transaction', function (done) {
    request(app)
      .post('/api/transactions')
      .send({
        type: 'credit',
        amount: 100
      })
      .end(function (err, res) {
        expect(res.statusCode).to.equal(201);
        transactionId = res.body.id;
        done();
      });
  });
  it('should get transaction history', function (done) {
    request(app)
      .get('/api/transactions')
      .end(function (err, res) {
        expect(res.statusCode).to.equal(200);
        expect(res.body.length).to.equal(2);
        done();
      });
  });  
});
