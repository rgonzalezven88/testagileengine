'use strict';

const express = require('express');
const router = express.Router();
const Controller = require('../controllers/transaction.controller');

/* GET get current balance */
router.get('/', Controller.getBalance);

/* GET get transactions history */
router.get('/transactions', Controller.getHistory);

/* POST create new transactions */
router.post('/transactions', Controller.create);

/* GET get transaction by ID */
router.get('/transactions/:id', Controller.getById);

module.exports = router;