const Joi = require('joi');
require('dotenv').config();

const envVarsSchema = Joi.object({
    NODE_ENV: Joi.string().allow(['development', 'test', 'production']).default('development'),
    PORT: Joi.number().default(8080)
}).unknown()
    .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
    throw new Error(`Config validation error: ${error.message}`);
}
const config = {
    env: envVars.NODE_ENV,
    port: envVars.PORT
};

module.exports = config;